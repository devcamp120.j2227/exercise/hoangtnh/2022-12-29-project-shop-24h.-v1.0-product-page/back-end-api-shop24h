const mongoose = require("mongoose");
const customerModel = require("../model/customerModel");
//fuction tạo thông tin customer mới
const createCustomer = (request, response) =>{
    const body = request.body;
    if(!body.UserId){
        return response.status(400).json({
            status:"Bad Request",
            message: "User Id không hợp lệ"
        })
    }
    const newCustomer = {
        UserId: body.UserId,
        Name: body.Name,
        Email: body.Email,
        Birthday: body.Birthday,
        Phone: body.Phone,
        Gender: body.Gender,
        Address: body.Address,
        Orders: body.Orders
    };
    customerModel.create(newCustomer,(error,data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            message:"Create new customer successfully",
            data: data
        })
    })
}
//fuction tạo thông tin customer mới trên admin page
const createCustomerAdminPage = (request, response) =>{
    const body = request.body;
    const newCustomer = {
        UserId: mongoose.Types.ObjectId(),
        Name: body.Name,
        Email: body.Email,
        Birthday: body.Birthday,
        Phone: body.Phone,
        Gender: body.Gender,
        Address: body.Address,
        Orders: body.Orders
    };
    customerModel.create(newCustomer,(error,data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            message:"Create new customer successfully",
            data: data
        })
    })
}
const getCustomerByUserId = (request, response) =>{
    const userId = request.query.userId;
    customerModel.findOne({UserId : userId})
        //.populate("Carts") load toàn bộ order của khách hàng bằng id order 
        .exec((error,data)=>{
            if(error){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else{
                return response.status(200).json({
                    status:"Get customer successfully",
                    data: data
                })
            }
        })
}
//get customer by email
const getCustomerByEmail = (request, response) =>{
    const email = request.query.email;
    customerModel.findOne({Email : email})
        //.populate("Carts") load toàn bộ order của khách hàng bằng id order 
        .exec((error,data)=>{
            if(error){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else{
                return response.status(200).json({
                    status:"Get customer by email successfully",
                    data: data
                })
            }
        })
}
//lấy thông tin người dùng và toàn bộ order của họ
const getCustomerAndOrdersByUserId = (request, response) =>{
    const userId = request.query.userId;
    customerModel.findOne({UserId : userId})
        .populate("Carts") //load toàn bộ order của khách hàng bằng id order 
        .exec((error,data)=>{
            if(error){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            }
            else{
                return response.status(200).json({
                    status:"Get customer successfully",
                    data: data
                })
            }
        })
}
const updateInforCustomer = (request, response) =>{
    const userId = request.query.userId;
    const body = request.body;
    if(!userId){
        return response.status(400).json({
            status: "Bad request",
            message: "User Id không đúng"
        })
    }
    const updateInfor = {};
    if(body.Address !== ""){
        updateInfor.Address = body.Address;
    }
    if(body.Name !== ""){
        updateInfor.Name = body.Name;
    }
    if(body.Birthday !== ""){
        updateInfor.Birthday = body.Birthday;
    }
    if(body.Phone !== ""){
        updateInfor.Phone = body.Phone;
    }
    if(body.Gender !== ""){
        updateInfor.Gender = body.Gender;
    }
    if(body.Orders !== ""){
        updateInfor.Orders = body.Orders;
    }
    customerModel.findOneAndUpdate({UserId : userId}, updateInfor,{new: true}, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        
        return response.status(201).json({
            status:"Update Infor customer successfully",
            data: data
        })
    })
}
const getAllCustomer = (req, res) =>{
    customerModel.find((error, data)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status:"Get all customers successfully",
            data: data
        })

    })
}
module.exports ={
    createCustomer,
    getCustomerByUserId,
    updateInforCustomer,
    getAllCustomer,
    getCustomerAndOrdersByUserId,
    createCustomerAdminPage,
    getCustomerByEmail
}