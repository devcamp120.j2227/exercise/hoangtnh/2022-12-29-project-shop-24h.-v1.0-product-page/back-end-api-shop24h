const jwtModel = require("../model/jwtModel");
require("dotenv").config();
const jwt = require("jsonwebtoken");
const { default: mongoose } = require("mongoose");
const bcrypt = require('bcrypt');
const customerModel = require("../model/customerModel");

const createNewAccount = (request, response) =>{
        const body = request.body;
        const hash = bcrypt.hashSync(body.password, 10);
        const newAccount = {
            _id: mongoose.Types.ObjectId(),//UserId of customer
            Username : body.username,
            Password : hash,
            Role: body.role,
            Email: body.email
        }

         jwtModel.create(newAccount, (err, data)=>{
            console.log(data)
            if(err){
                return response.status(500).json({
                    status:"Internal server error",
                    message: err.message
                })
            }
            if(data){
                const newCustomer = {
                    UserId: data._id,
                    Name: data.Username,
                    Email: data.Email,
                };
                customerModel.create(newCustomer,(error,newCustomer)=>{
                        if(error){
                            return response.status(500).json({
                                status:"Internal server error",
                                message: error.message
                            })
                        }
                        if(newCustomer){
                            console.log(newCustomer)
                            return response.status(201).json({
                                message:"Create new customer successfully",
                                data: newCustomer
                            })
                        }
                    })
                // return response.status(201).json({
                //     message:"Create new account successfully",
                //     Account: data
                // })
            }
            
        })
        
}
const loginAccountWithEncodePass = (req, res)=>{
    const username = req.body.username;
    const password = req.body.password;
    
     jwtModel.findOne({Username: username},(error,exitUser)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: "Error"
            })
        }
        if(!exitUser){
            return res.status(500).json({
                status:"Internal server error",
                message: "username is incorrect"
            })
        }
        if(exitUser){
            // Display the hashed password
            // Use bcrypt.compare() function to compare
            // the password with hashed password
            bcrypt.compare(password, exitUser.Password, (error, data) => {
            //bcrypt.compare(exitUser.Password, password , (error, data) => {
                if( error ) {
                    return res.status(500).json({
                        status:"Internal server error",
                        message: "Error"
                    });
                }
            // If password matched
                if (data) {
                    // Send JWT
                    //tạo access token và refresh token mới
                    const accessToken = jwt.sign({username: exitUser.Username}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"12h"});
                    const refreshToken =  jwt.sign({username: exitUser.Username}, process.env.REFRESH_TOKEN_SECRET);
                    //đẩy refresh token lên database
                    jwtModel.findOneAndUpdate({Username: exitUser.Username}, {RefToken: refreshToken, AccessToken: accessToken}, {new: true},(error, data)=>{
                        console.log(error, data)
                        
                        
                    });
                    if(exitUser.Role === "admin" || exitUser.Role === "staff"){
                        return res.status(201).json({
                            message:"Login as admin",
                            data: {accessToken, refreshToken, exitUser} 
                        });
                    }
                    else{
                        return res.status(201).json({
                            message:"true password, Login successful",
                            data: {accessToken, refreshToken, exitUser} 
                        });
                    }
                    } else {
                    // response is OutgoingMessage object that server response http request
                    return res.status(404).json({
                        success: false, 
                        message: 'Password is incorrect'
                    });
                    }
            });
        }
    })
}
const editPassword = (req,res)=>{
    const username = req.body.username;
    const newPass = req.body.newPass;
    console.log(process.env.SALT_WORK_FACTOR)
    const hash = bcrypt.hashSync(newPass, 10);
    jwtModel.findOneAndUpdate({Username: username}, {Password: hash}, {new:true}, (error, data)=>{
        if(error){
            return res.status(500).json({
                status:"Internal server error",
                message: "Error"
            });
        }
        return res.status(200).json({
            message: 'update success',
            data: data
        });
    })
}
//method xử lý login account
const loginAccount = (request, response) =>{
    const username =  request.body.username;
    const password = request.body.password;
    
    jwtModel.findOne({Username : username}, (error, exitsUser) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: "Error"
            })
        }
        //trường hợp username không đúng
        if(!exitsUser){
            return response.status(500).json({
                status:"Internal server error",
                message: "username is incorrect"
            })
        }
        //trường hợp username đúng kiểm trả password
        if(exitsUser){
                if(exitsUser.Password !== password){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: "Password is incorrect"
                    })
                }
                //nếu password đúng
                if(exitsUser.Password === password){
                    if(exitsUser.Role === "admin" || exitsUser.Role === "staff"){
                        //tạo access token và refresh token mới
                        const accessToken = jwt.sign({username: exitsUser.Username}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"1h"});
                        const refreshToken =  jwt.sign({username: exitsUser.Username}, process.env.REFRESH_TOKEN_SECRET);
                        //đẩy refresh token lên database
                        jwtModel.findOneAndUpdate({Username: exitsUser.Username}, {RefToken: refreshToken, AccessToken: accessToken}, {new: true},(error, data)=>{
                            console.log(error, data)
                        });
                        const admin = {
                          username : exitsUser.Username,
                          role : exitsUser.Role
                        }
                        return response.status(201).json({
                            message:"Login as admin",
                            data: {accessToken, refreshToken, admin} 
                        });
                    }
                    if(exitsUser.Role !== "staff" && exitsUser.Role !== "admin"){
                        return response.status(201).json({
                            message:"Log in successfully",
                            data: exitsUser
                        })
                    }
                }
        }  
    })
}
//lấy refresh token để tạo accesstoken mới
const refreshToken = (request, response) =>{
    const refToken = request.body.token;
    if(!refToken){
        return response.status(500).json({
            status:"Internal server error",
        })
    }
    jwtModel.findOne({RefToken: refToken}, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: "RefToken is incorrect"
            })
        }
        const accessToken = jwt.sign({username: data.Username}, process.env.ACCESS_TOKEN_SECRET, {expiresIn:"1h"});
        return response.status(201).json({
            message:"get access token successfully",
            data: {accessToken}
        }) 
    })
    
}
//log out admin thì xóa đi refresh token
const logoutAccount = (request, response) => {
    const refToken = request.body.token;
    jwtModel.findOneAndUpdate({RefToken: refToken},{RefToken: null}, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        if(!data){
            return response.status(401).json({
                message:"RefToken is incorrect",
            }) 
        }
        return response.status(201).json({
            message:"Log out successfully",
        }) 
    })
}
//method chỉnh sửa quyền trên admin page
const editAdministrator = (request, response) =>{
    const username = request.params.username;
    const role = request.body.role;

    jwtModel.findOneAndUpdate({Username: username}, {Role: role}, {new: true}, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        if(!data){
            return response.status(401).json({
                message:"username is not found",
            }) 
        }
        return response.status(201).json({
            message:"admin premission has changed",
            data: data
        }) 
    })
}
const getAllAccount = (request, response) => {
    jwtModel.find((error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all account successfully",
            data: data
        })
    })
}
// const getAccountByUsername =(request, response)=> {
//     const username = request.query.username;

//     jwtModel.find({Username : username}, (error, data)=>{
//         if(error){
//             return response.status(500).json({
//                 status:"Internal server error",
//                 message: error.message
//             })
//         }
//         return response.status(200).json({
//             status:`Get account successfully`,
//             data: data
//         })
//     })
// }
module.exports ={
    createNewAccount,
    getAllAccount,
    loginAccount,
    refreshToken, 
    logoutAccount, 
    editAdministrator,
    loginAccountWithEncodePass,
    editPassword
}