const express = require("express");
const router = express.Router();

const customerController = require("../controllers/customerController");

router.post("/customers", customerController.createCustomer);
router.post("/customers-admin-page", customerController.createCustomerAdminPage)
router.get("/customers", customerController.getCustomerByUserId);
router.put("/customers", customerController.updateInforCustomer);
router.get("/allcustomers", customerController.getAllCustomer);
router.get("/customer&orders", customerController.getCustomerAndOrdersByUserId);
router.get("/email-customer", customerController.getCustomerByEmail);

module.exports = router;