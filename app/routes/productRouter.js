const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const jwtMiddleware =  require("../middlewares/jwtMiddleware");

router.post("/products", productController.createProduct);
router.get("/products",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, productController.getAllProduct);
router.get("/products/description", productController.getProductByDescription);
router.get("/products/filter", productController.getProductByCondition);
router.get("/limit-products",productController.getProductLimit);
router.get("/skip-limit-products", productController.getProductsSkipLimit);
router.get("/products/:productId", productController.getProductById);
router.put("/products/:productId",jwtMiddleware.authenAdminToken, jwtMiddleware.verifyAdmin, productController.editProduct);
module.exports = router;