const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");

router.post("/orders/post", orderController.createOrder);
router.get("/orders/:orderId", orderController.getOrderById);
router.get("/orders", orderController.getAllOrder);
router.get("/ordersCustomer/:orderId", orderController.findCustomerInfoByOrderId);
router.get("/orders/filter/:orderId", orderController.filterOrder);
router.get("/filter", orderController.filterOrderByStatus);
router.put("/orders/edit", orderController.updateStatusOrder);
router.delete("/orders/delete", orderController.deleteOrder);
router.get("/orders/phoneNumber/:phone", orderController.findOrderByPhoneNumber);
router.get("/filterDate/dateOrder", orderController.findOrderByDate);
module.exports = router;