const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const jwtSchema = new Schema ({
    Username :{
        type: String,
        require: true,
        unique: true
    },
    Password: {
        type: String,
        require: true
    },
    Email:{
        type: String,
        unique: true,
        require: true
    },
    Role: {
        type: String
    },
    AccessToken:{
        type:String
    },
    RefToken: {
        type: String
    },
    Information: {
        type: mongoose.Types.ObjectId,
        ref: "Customers"
    }
},{timestamps: true});

module.exports = mongoose.model("Jwt", jwtSchema);