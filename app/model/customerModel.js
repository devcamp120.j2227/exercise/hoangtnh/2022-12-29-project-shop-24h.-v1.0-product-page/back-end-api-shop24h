const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const customerSchema = new Schema ({
    UserId :{
        type: String,
        unique: true
    },
    Name: {
        type: String,
        require: true
    },
    Email:{
        type: String,
        require: true,
        unique: true
    },
    Birthday:{
        type: String,
    },
    Phone:{
        type: String,
        require: true
    },
    Gender:{
        type: String
    },
    Address:{
        type: {},
        require: true
    },
    Orders:{
        type: []
    },
    Carts:[{
        type: mongoose.Types.ObjectId,
        ref: "Orders"
    }]
});
module.exports = mongoose.model("Customers", customerSchema);